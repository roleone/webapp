# RoleOne

## Angular Version: 6.0.8

### NPM Packages required commands

```shell
npm install
```

### Running SonarQube

```shell
tslint --project tsconfig.json -c tslint.json 'src/**/*.ts'
```